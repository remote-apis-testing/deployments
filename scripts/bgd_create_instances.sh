#!/bin/bash

# Creates AWS instances for a BuildGrid test
#
# The '-c' option is the number of Bazel client instances
# The '-k' option is the name of the AWS key pair
# The '-w' option is the number of BuildGrid worker instances
#

# Defaults
CLIENT_COUNT=1
WORKER_COUNT=1
KEY_NAME="rdale"

while getopts "c:k:w:" opt; do
  case ${opt} in
    c )
      CLIENT_COUNT=$OPTARG
     ;;
    k )
      KEY_NAME=$OPTARG
     ;;
    w )
      WORKER_COUNT=$OPTARG
     ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: '$OPTARG' requires an argument" 1>&2
      ;;
  esac
done

# Ubuntu Server 18.04 LTS (HVM), SSD Volume Type - ami-07dc734dc14746eab

TEST_RUN=$(aws ec2 describe-security-groups --group-name bazel-test --query "SecurityGroups[*].[Tags[?Key=='TestRun'].Value]" --output text)
TEST_RUN=$(($TEST_RUN + 1))
aws ec2 create-tags --resources sg-0dd6e7462e21a29eb --tags "Key=TestRun,Value=$TEST_RUN"

BGD_SERVER=$(aws ec2 run-instances --image-id  ami-07dc734dc14746eab --instance-type c5d.2xlarge \
   --security-groups bazel-test --key-name "$KEY_NAME" \
   --tag-specifications "ResourceType=instance,Tags=[{Key=Purpose,Value=bazel-test},{Key=TestComponent,Value=bgd-server},{Key=TestRun,Value=$TEST_RUN}]" \
   --query 'Instances[0].InstanceId' --output text)
echo "bgd_server instance: $BGD_SERVER"

BGD_WORKERS=$(aws ec2 run-instances --image-id  ami-07dc734dc14746eab --count $WORKER_COUNT --instance-type c5.4xlarge \
   --security-groups bazel-test --key-name "$KEY_NAME" \
   --tag-specifications "ResourceType=instance,Tags=[{Key=Purpose,Value=bazel-test},{Key=TestComponent,Value=bgd-worker},{Key=TestRun,Value=$TEST_RUN}]" \
   --query 'Instances[0].InstanceId' --output text)
echo "bgd_worker instances: $BGD_WORKERS"

BAZEL_CLIENTS=$(aws ec2 run-instances --image-id  ami-07dc734dc14746eab --count $CLIENT_COUNT --instance-type c5.large \
   --security-groups bazel-test --key-name "$KEY_NAME" \
   --tag-specifications "ResourceType=instance,Tags=[{Key=Purpose,Value=bazel-test},{Key=TestComponent,Value=bazel-client},{Key=TestRun,Value=$TEST_RUN}]" \
   --query 'Instances[0].InstanceId' --output text)
echo "bazel client instances: $BAZEL_CLIENTS"
