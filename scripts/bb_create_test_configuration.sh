#!/bin/bash

# Creates a BuildBarn test configuration with AWS instances and
# deploys Ansible configurations to them
#
# The '-c' option specifies the number of clients
# The '-w' option specifies the number of workers
#
WORKER_COUNT=1
CLIENT_COUNT=1

while getopts "c:w:" opt; do
  case ${opt} in
    c )
      CLIENT_COUNT=$OPTARG
     ;;
    w )
      WORKER_COUNT=$OPTARG
     ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: '$OPTARG' requires an argument" 1>&2
      ;;
  esac
done

DIR="$(cd "$(dirname "$0")" && pwd)"
ANSIBLE_DIR=$DIR/../ansible

GROUP_ID=$(aws ec2 describe-security-groups --group-name bazel-test --query 'SecurityGroups[0].GroupId' --output text)
if [ -z "$GROUP_ID" ] ; then
    echo "Creating bazel-test security group"
    $DIR/create_security_group.sh
fi

$DIR/bb_create_instances.sh -w$WORKER_COUNT -c$CLIENT_COUNT

# Allow the instances time to start up
echo "Waiting for instances to start up"
sleep 120

ANSIBLE_INVENTORY=`$DIR/bb_create_inventory.sh`
echo "Ansible inventory: $ANSIBLE_INVENTORY"

export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook -i "$ANSIBLE_INVENTORY" $ANSIBLE_DIR/bb-storage.yml
ansible-playbook -i "$ANSIBLE_INVENTORY" $ANSIBLE_DIR/bb-scheduler.yml
ansible-playbook -i "$ANSIBLE_INVENTORY"  $ANSIBLE_DIR/bb-worker.yml
ansible-playbook -i "$ANSIBLE_INVENTORY" $ANSIBLE_DIR/bazel-client.yml
