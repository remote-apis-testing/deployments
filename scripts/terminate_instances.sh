#!/bin/bash

# Terminates the set of AWS instances for a test run, the default is
# the current test run.
# The '-r' specifies a particular test run number


while getopts "r:" opt; do
  case ${opt} in
    r )
      TEST_RUN=$OPTARG
     ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: '$OPTARG' requires an argument" 1>&2
      ;;
  esac
done

if [ -z "$TEST_RUN" ] ; then
    TEST_RUN=$(aws ec2 describe-security-groups --group-name bazel-test --query "SecurityGroups[*].[Tags[?Key=='TestRun'].Value]" --output text)
fi

INSTANCES=$(aws ec2 describe-instances \
    --filters "Name=tag:TestRun,Values=$TEST_RUN" 'Name=instance-state-name,Values=running' \
    --query 'Reservations[*].Instances[*].[InstanceId]' --output text)

for INSTANCE in $INSTANCES ; do
	aws ec2 terminate-instances --instance-ids $INSTANCE
done
