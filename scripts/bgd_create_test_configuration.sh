#!/bin/bash

# Creates a BuildGrid test configuration with AWS instances and
# deploys Ansible configurations to them
#
# The '-c' option specifies the number of clients
# The '-w' option specifies the number of workers
#

WORKER_COUNT=1
CLIENT_COUNT=1
WORKERS_PER_MACHINE=8

while getopts "c:m:w:" opt; do
  case ${opt} in
    c )
      CLIENT_COUNT=$OPTARG
     ;;
    m )
      WORKERS_PER_MACHINE=$OPTARG
     ;;
    w )
      WORKER_COUNT=$OPTARG
     ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: '$OPTARG' requires an argument" 1>&2
      ;;
  esac
done

DIR="$(cd "$(dirname "$0")" && pwd)"
ANSIBLE_DIR=$DIR/../ansible

GROUP_ID=$(aws ec2 describe-security-groups --group-name bazel-test --query 'SecurityGroups[0].GroupId' --output text)
if [ -z "$GROUP_ID" ] ; then
    echo "Creating bazel-test security group"
    $DIR/create_security_group.sh
fi

$DIR/bgd_create_instances.sh -w$WORKER_COUNT -c$CLIENT_COUNT

# Allow the instances time to start up
echo "Waiting for instances to start up"
sleep 120

ANSIBLE_INVENTORY=`$DIR/bgd_create_inventory.sh`
echo "Ansible inventory: $ANSIBLE_INVENTORY"

export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook -i "$ANSIBLE_INVENTORY" $ANSIBLE_DIR/bgd-server.yml
ansible-playbook -i "$ANSIBLE_INVENTORY" --extra-vars "bgd_workers_per_machine=$WORKERS_PER_MACHINE" $ANSIBLE_DIR/bgd-worker.yml
ansible-playbook -i "$ANSIBLE_INVENTORY" $ANSIBLE_DIR/bazel-client.yml
