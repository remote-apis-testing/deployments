#!/bin/bash

aws ec2 create-security-group --group-name bazel-test --description "Bazel testing security group"
aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 50051 --cidr 0.0.0.0/0

aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 7980 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 7981 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 7983 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 7984 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 7985 --cidr 0.0.0.0/0

aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 8980 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 8981 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name bazel-test --protocol tcp --port 8983 --cidr 0.0.0.0/0

# Each time a configuration of instances for a test is created, the 'TestRun' tag is used to identify
# them as belonging to a particular test run. 'TestRun' is incremented by 1 before each time it is used.
GROUP_ID=$(aws ec2 describe-security-groups --group-name bazel-test    --query 'SecurityGroups[0].GroupId' --output text)
aws ec2 create-tags --resources $GROUP_ID --tags "Key=TestRun,Value=1"
