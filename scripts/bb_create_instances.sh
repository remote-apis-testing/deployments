#!/bin/bash

# Creates AWS instances for a BuildBarn test
#
# The '-c' option is the number of Bazel client instances
# The '-k' option is the name of the AWS key pair
# The '-w' option is the number of BuildBarn worker instances
#

# Defaults
CLIENT_COUNT=1
WORKER_COUNT=1
KEY_NAME="rdale"

while getopts "c:k:w:" opt; do
  case ${opt} in
    c )
      CLIENT_COUNT=$OPTARG
     ;;
    w )
      WORKER_COUNT=$OPTARG
     ;;
    k )
      KEY_NAME=$OPTARG
     ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: '$OPTARG' requires an argument" 1>&2
      ;;
  esac
done

# Ubuntu Server 18.04 LTS (HVM), SSD Volume Type - ami-07dc734dc14746eab

TEST_RUN=$(aws ec2 describe-security-groups --group-name bazel-test --query "SecurityGroups[*].[Tags[?Key=='TestRun'].Value]" --output text)
TEST_RUN=$(($TEST_RUN + 1))
aws ec2 create-tags --resources sg-0dd6e7462e21a29eb --tags "Key=TestRun,Value=$TEST_RUN"

BB_STORAGE=$(aws ec2 run-instances --image-id  ami-07dc734dc14746eab --instance-type c5d.2xlarge \
   --security-groups bazel-test --key-name "$KEY_NAME" \
   --tag-specifications "ResourceType=instance,Tags=[{Key=Purpose,Value=bazel-test},{Key=TestComponent,Value=bb-storage},{Key=TestRun,Value=$TEST_RUN}]" \
   --query 'Instances[0].InstanceId' --output text)
echo "bb_storage instance: $BB_STORAGE"

BB_SCHEDULER=$(aws ec2 run-instances --image-id  ami-07dc734dc14746eab --instance-type c5.large \
   --security-groups bazel-test --key-name "$KEY_NAME" \
   --tag-specifications "ResourceType=instance,Tags=[{Key=Purpose,Value=bazel-test},{Key=TestComponent,Value=bb-scheduler},{Key=TestRun,Value=$TEST_RUN}]" \
   --query 'Instances[0].InstanceId' --output text)
echo "bb_scheduler instance: $BB_SCHEDULER"

BB_WORKERS=$(aws ec2 run-instances --image-id  ami-07dc734dc14746eab --count $WORKER_COUNT --instance-type c5.4xlarge \
   --security-groups bazel-test --key-name "$KEY_NAME" \
   --tag-specifications "ResourceType=instance,Tags=[{Key=Purpose,Value=bazel-test},{Key=TestComponent,Value=bb-worker},{Key=TestRun,Value=$TEST_RUN}]" \
   --query 'Instances[0].InstanceId' --output text)
echo "bb_worker instances: $BB_WORKERS"

BAZEL_CLIENTS=$(aws ec2 run-instances --image-id  ami-07dc734dc14746eab --count $CLIENT_COUNT --instance-type c5.large \
   --security-groups bazel-test --key-name "$KEY_NAME" \
   --tag-specifications "ResourceType=instance,Tags=[{Key=Purpose,Value=bazel-test},{Key=TestComponent,Value=bazel-client},{Key=TestRun,Value=$TEST_RUN}]" \
   --query 'Instances[0].InstanceId' --output text)
echo "bazel client instances: $BAZEL_CLIENTS"
