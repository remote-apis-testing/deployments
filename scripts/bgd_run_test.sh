#!/bin/bash

# Builds a target on each Bazel client in a BuildGrid test run
# configuration.
#
# The current test run is the default
# The '-r' option specifies a particular test run number
# The '-t' option specifies a particular target, either 'abseil-cpp'
# or 'tensor-flow'

# Default
TEST_TARGET="abseil-cpp"
JOBS="8"

while getopts "j:r:t:" opt; do
  case ${opt} in
    j )
      JOBS=$OPTARG
     ;;
    t )
      TEST_TARGET=$OPTARG
     ;;
    t )
      TEST_TARGET=$OPTARG
     ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: '$OPTARG' requires an argument" 1>&2
      ;;
  esac
done

if [ -z "$TEST_RUN" ] ; then
    TEST_RUN=$(aws ec2 describe-security-groups --group-name bazel-test --query "SecurityGroups[*].[Tags[?Key=='TestRun'].Value]" --output text)
fi

DIR="$(cd "$(dirname "$0")" && pwd)"
ANSIBLE_DIR=$DIR/../ansible

ANSIBLE_INVENTORY=`$DIR/bgd_create_inventory.sh -r$TEST_RUN`
echo "Ansible inventory: $ANSIBLE_INVENTORY"

BAZEL_CLIENT_IP_ADDRS=$(aws ec2 describe-instances \
        --filters "Name=tag:TestRun,Values=$TEST_RUN" 'Name=tag:TestComponent,Values=bazel-client'  'Name=instance-state-name,Values=running' \
        --query 'Reservations[*].Instances[*].PublicIpAddress' --output text)

CLIENT_COUNT=`echo $BAZEL_CLIENT_IP_ADDRS | awk '{ print NF }'`

TEST_RUN_OUTPUT="/tmp/bgd_test_output_$TEST_RUN.txt"
export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook -i "$ANSIBLE_INVENTORY" "$ANSIBLE_DIR/bgd-build-$TEST_TARGET.yml" --fork "$CLIENT_COUNT" --extra-vars "jobs=$JOBS" 2>&1 | tee "$TEST_RUN_OUTPUT"
echo "Test output: $TEST_RUN_OUTPUT"
