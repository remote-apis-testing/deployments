#!/bin/bash

# Creates an Ansible inventory for a BuildGrid test
#
# The '-r' option is the test run number
# The '-9' option is the name of the output file
#

while getopts "r:o:" opt; do
  case ${opt} in
    r )
      TEST_RUN=$OPTARG
     ;;
    o )
      ANSIBLE_INVENTORY=$OPTARG
      ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: '$OPTARG' requires an argument" 1>&2
      ;;
  esac
done

if [ -z "$TEST_RUN" ] ; then
    TEST_RUN=$(aws ec2 describe-security-groups --group-name bazel-test --query "SecurityGroups[*].[Tags[?Key=='TestRun'].Value]" --output text)
fi

if [ -z "$ANSIBLE_INVENTORY" ] ; then
    ANSIBLE_INVENTORY="/tmp/bgd_hosts_$TEST_RUN"
fi

BGD_SERVER_IP_ADDR=$(aws ec2 describe-instances \
        --filters "Name=tag:TestRun,Values=$TEST_RUN" 'Name=tag:TestComponent,Values=bgd-server'  'Name=instance-state-name,Values=running' \
        --query 'Reservations[*].Instances[*].PublicIpAddress' --output text)

BGD_WORKER_IP_ADDRS=$(aws ec2 describe-instances \
        --filters "Name=tag:TestRun,Values=$TEST_RUN" 'Name=tag:TestComponent,Values=bgd-worker'  'Name=instance-state-name,Values=running' \
        --query 'Reservations[*].Instances[*].PublicIpAddress' --output text)

BAZEL_CLIENT_IP_ADDRS=$(aws ec2 describe-instances \
        --filters "Name=tag:TestRun,Values=$TEST_RUN" 'Name=tag:TestComponent,Values=bazel-client'  'Name=instance-state-name,Values=running' \
        --query 'Reservations[*].Instances[*].PublicIpAddress' --output text)

echo "all:" > $ANSIBLE_INVENTORY
echo "  children:" >> $ANSIBLE_INVENTORY

echo "    bgd-server:" >> $ANSIBLE_INVENTORY
echo "      hosts:" >> $ANSIBLE_INVENTORY
echo "        bgd_server0:" >> $ANSIBLE_INVENTORY
echo "          ansible_ssh_host: $BGD_SERVER_IP_ADDR" >> $ANSIBLE_INVENTORY
echo "          ansible_ssh_user: ubuntu" >> $ANSIBLE_INVENTORY

echo "    bgd-workers:" >> $ANSIBLE_INVENTORY
echo "      hosts:" >> $ANSIBLE_INVENTORY
WORKER_COUNT=0
for BGD_WORKER_IP_ADDR in $BGD_WORKER_IP_ADDRS ; do
    echo "        bgd_worker$WORKER_COUNT:" >> $ANSIBLE_INVENTORY
    echo "          ansible_ssh_host: $BGD_WORKER_IP_ADDR" >> $ANSIBLE_INVENTORY
    echo "          ansible_ssh_user: ubuntu" >> $ANSIBLE_INVENTORY
    WORKER_COUNT=$(($WORKER_COUNT + 1))
done

echo "    bazel-clients:" >> $ANSIBLE_INVENTORY
echo "      hosts:" >> $ANSIBLE_INVENTORY
BAZEL_CLIENT_COUNT=0
for BAZEL_CLIENT_IP_ADDR in $BAZEL_CLIENT_IP_ADDRS ; do
    echo "        bazel_client$BAZEL_CLIENT_COUNT:" >> $ANSIBLE_INVENTORY
    echo "          ansible_ssh_host: $BAZEL_CLIENT_IP_ADDR" >> $ANSIBLE_INVENTORY
    echo "          ansible_ssh_user: ubuntu" >> $ANSIBLE_INVENTORY
    BAZEL_CLIENT_COUNT=$(($BAZEL_CLIENT_COUNT + 1))
done

echo "$ANSIBLE_INVENTORY"
