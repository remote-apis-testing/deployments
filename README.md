# AWS/Ansible Deployment Scripts

## Creating AWS Testing Configurations

The tests use an AWS security group called 'bazel-test', which is tagged with a 'TestRun' tag with an initial value of 0.
The security group makes the various ports used by BuildBarn/BuildGrid publically accessible.

Each time a configuration of instances for a test is created, the 'TestRun' tag is used to identify them as belonging to a particular test run.
'TestRun' is incremented by 1 before each time it is used.
A particular value of TestRun can be used to retrieve the host addresses of a set of AWS instances used in a test.

Each AWS instance is given a 'TestComponent' tag with values such as 'bgd-server' or 'bb-worker'.
These tags are used in conjunction with the 'TestRun' tag to create Ansible inventories for the IP addresses of the hosts used in a test configuration.

### create_security_group.sh

Creates the 'bazel-test' security group with a tag of 'TestRun', and 'TestRun' has an initial value of zero.
The ports used by the various BuildGrid and BuildBarn components are made public for members of the security group.

Example usage:

    $ ./scripts/create_security_group.sh

### terminate_instances.sh

Kills all AWS instances with the current 'TestRun' tag.
An optional '-r' argument can be used to specify a particular 'TestRun'.

Example usage:

    $ ./scripts/terminate_instances.sh -r5

Terminates all AWS instances tagged with a 'TestRun' of 5.

## BuildBarn scripts

The following AWS EC2 models are created by the scripts as instances:

* Storage: c5d.2xlarge
* Scheduler: c5.large
* Workers: c5.4xlarge
* Bazel clients: c5.large

### bb_create_instances.sh

Creates all the AWS instances needed for a BuildBarn test. Each instance is given a 'TestRun' and 'TestComponent' tag.

Example usage:

    $ ./scripts/bgd_create_instances.sh -kjsmith -w8 -c4

Note: You will not normally need to call this script directly.

### bb_create_inventory.sh

Creates an Ansible inventory for a particular BuildBarn test configuration.

An optional '-r' argument allows a particular value of the 'TestRun' tag to be specified.
An optional '-o' argument allows the name of the inventory file to be specified.

Example usage:

    $ ./scripts/bb_create_inventory -r6 -o"my_bb_inventory"


Note: You will not normally need to call this script directly.

### bb_create_test_configuration.sh

Uses the 'bb_create_instances.sh' script to create a set of AWS instances.
An Ansible inventory is created with 'bb_create_inventory.sh' and used to deploy resources needed for a BuildBarn test onto the AWS instances.

A '-k' argument gives the AWS key pair name. An optional '-w' argument allows the number of workers to be specified, and an optional '-c' argument specifies the number of Bazel client instances.

Example usage:

    $ ./scripts/bb_create_test_configuration.sh -kjsmith -w4 -c4

Creates a test configuration with 4 BuildBarn workers and 4 client instances.
The AWS key pair 'jsmith' is used.

### bb_run_test.sh

Runs a test Bazel build on each client instance. By default it will run an abseil-cpp build, but an optional '-t' argument can specify a different target of 'tensor-flow'.

Example usage:

    $ ./scripts/bb_run_test.sh -j8 -ttensor-flow

Runs builds of TensorFlow on each client at the same time with '--jobs=8'.

## BuildGrid scripts

The following AWS EC2 models are created by the scripts as instances:

* Server: c5d.2xlarge
* Workers: c5.4xlarge
* Bazel clients: c5.large

### bgd_create_instances.sh

Creates all the AWS instances needed for a BuildGrid test. Each instance is given a 'TestRun' and 'TestComponent' tag.

Example usage:

    $ ./scripts/bgd_create_instances.sh -kjsmith -w8 -c4

Note: You will not normally need to call this script directly.

### bgd_create_inventory.sh

Creates an Ansible inventory for a particular BuildGrid test configuration.

An optional '-r' argument allows a particular value of the 'TestRun' tag to be specified.
An optional '-o' argument allows the name of the inventory file to be specified.

Example usage:

    $ ./scripts/bgd_create_inventory -r6 -o"my_bgd_inventory"

Note: You will not normally need to call this script directly.

### bgd_create_test_configuration.sh

Uses the 'bgd_create_instances.sh' script to create a set of AWS instances.
An Ansible inventory is created with 'bgd_create_inventory.sh' and used to deploy resources needed for a BuildGrid test onto the AWS instances.

A '-k' argument gives the AWS key pair name. An optional '-w' argument allows the number of worker instances to be specified, and an optional '-m' argument specifies the number of worker processes on each instance. An optional '-c' argument specifies the number of Bazel client instances.

Example usage:

    $ ./scripts/bgd_create_test_configuration.sh -kjsmith -w4 -m8 -c4

Creates a test configuration with 4 BuildGrid worker instances, and 8 worker bot processes on each instance, along with 4 Bazel client machine instances.
The AWS key pair 'jsmith' is used.

### bgd_run_test.sh

Runs a test Bazel build on each client instance. By default it will run an abseil-cpp build, but an optional '-t' argument can specify a different target of 'tensor-flow'.
The value of the Bazel '--jobs' option can be specified with a '-j' argument.

Example usage:

    $ ./scripts/bgd_run_test.sh -j8 -ttensor-flow

Runs builds of TensorFlow on each client at the same time with '--jobs=8'.
